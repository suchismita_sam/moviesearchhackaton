//
//  DestinationController.m
//  movieSearchHackaton
//
//  Created by Click Labs134 on 11/27/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "DestinationController.h"
AFHTTPRequestOperation *operation;
@interface DestinationController ()
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *releaseDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *actorsLabel;
@property (strong, nonatomic) IBOutlet UILabel *directorsLabel;
@property (strong, nonatomic) IBOutlet UILabel *writersLAbel;
@property (strong, nonatomic) IBOutlet UILabel *genreLabel;
@property (strong, nonatomic) IBOutlet UILabel *runTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *ratedLabel;
@property (strong, nonatomic) IBOutlet UILabel *languageLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *poster;

@end

@implementation DestinationController
@synthesize destdata;
@synthesize titleLabel;
@synthesize releaseDateLabel;
@synthesize actorsLabel;
@synthesize directorsLabel;
@synthesize writersLAbel;
@synthesize genreLabel;
@synthesize ratedLabel;
@synthesize runTimeLabel;
@synthesize languageLabel;
@synthesize descriptionLabel;
NSURLRequest *request;
@synthesize poster;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *showTitle=[destdata objectAtIndex:0];
    titleLabel.text=[NSString stringWithFormat:@"%@",showTitle];
    
    NSString *showDescription=[destdata objectAtIndex:1];
    descriptionLabel.text=[NSString stringWithFormat:@"%@",showDescription];
    
    NSString *showReleaseDate=[destdata objectAtIndex:2];
    releaseDateLabel.text=[NSString stringWithFormat:@"%@",showReleaseDate];
    
    NSString *actorsName=[destdata objectAtIndex:3];
    actorsLabel.text=[NSString stringWithFormat:@"%@",actorsName];
    
    NSString *showDirector=[destdata objectAtIndex:6];
    directorsLabel.text=[NSString stringWithFormat:@"%@",showDirector];
    
    NSString *showGenre=[destdata objectAtIndex:7];
    genreLabel.text=[NSString stringWithFormat:@"%@",showGenre];
    
    NSString *showLanguage=[destdata objectAtIndex:8];
    languageLabel.text=[NSString stringWithFormat:@"%@",showLanguage];
    
    NSString *showRunTime=[destdata objectAtIndex:9];
    runTimeLabel.text=[NSString stringWithFormat:@"%@",showRunTime];
    
    NSString *showWriter=[destdata objectAtIndex:10];
    writersLAbel.text=[NSString stringWithFormat:@"%@",showWriter];
    
    NSString *showRating=[destdata objectAtIndex:11];
    ratedLabel.text=[NSString stringWithFormat:@"%@",showRating];
    
    NSString *pic=[destdata objectAtIndex:1];
    NSURL *url=[NSURL URLWithString:pic];
    request=[NSURLRequest requestWithURL:url];
    operation=[[AFHTTPRequestOperation alloc]initWithRequest:request];
    operation.responseSerializer=[AFImageResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject)
     {
         poster=[[UIImageView alloc]init];
         poster.image=responseObject;
     }failure:^(AFHTTPRequestOperation *operation,NSError *error)
     {
         NSLog(@"Error:%@",error);
     }];
    [operation start];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
