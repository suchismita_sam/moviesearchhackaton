//
//  ViewController.m
//  movieSearchHackaton
//
//  Created by Click Labs134 on 11/27/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSMutableArray *result;
NSString *searchQuery;
NSString* searchText;
NSPredicate *resultPredicate;
NSString* data;
NSURLRequest *request;
NSURL *url;
NSMutableArray *showMovieTitleArray;
NSMutableArray *showMoviePicArray;
NSMutableArray *showMovieDetailsArray;
NSMutableArray *showMovieDescription;
NSMutableArray *showMovieReleaseDate;
NSMutableArray *destData;
NSString *picURL;
NSURLRequest *request;
int count;
UIImageView *moviePoster;
UITableView* cell;

DestinationController* destinationControllerObject;
@interface ViewController ()

@end

@implementation ViewController
@synthesize table;
@synthesize search;
- (void)viewDidLoad {
    [super viewDidLoad];
    count=0;
    result=[[NSMutableArray alloc]init];
    showMovieTitleArray=[[NSMutableArray alloc]init];
    showMoviePicArray=[[NSMutableArray alloc]init];
    showMovieDescription=[[NSMutableArray alloc]init];
    showMovieReleaseDate=[[NSMutableArray alloc]init];
    showMovieDetailsArray=[[NSMutableArray alloc]init];
    table.hidden=YES;
    [table reloadData];

    // Do any additional setup after loading the view, typically from a nib.
}

-(void) getApi
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // data is a string
    NSLog(@"hello");
    NSLog(@"%@",data);
    
    NSString *urlString;
    urlString = [data stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    [manager GET:[NSString stringWithFormat:@"http://www.omdbapi.com/?t=%@",urlString]
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id json)
    {
              if (json)
              {
                  NSLog(@"%@",json);
                  
                  //title
                  NSString *actorDictionary=json[@"Title"];
                  [showMovieTitleArray addObject:actorDictionary];
                  [showMovieDetailsArray addObject:actorDictionary];

                  
                  //poster
                  NSString *imageDictionary=json[@"Poster"];
                  picURL=[NSString stringWithFormat:@"%@",imageDictionary];
                 // NSLog(@"%@",picURL);
                  if ([picURL isEqualToString:@"N/A"])
                  {
                      moviePoster.image=[UIImage imageNamed:
                                                     @"na.jpg"];
                  }
                  NSURL *url=[NSURL URLWithString:picURL];
                  request=[NSURLRequest requestWithURL:url];
                  operation=[[AFHTTPRequestOperation alloc]initWithRequest:request];
                  operation.responseSerializer=[AFImageResponseSerializer serializer];
                  [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation,id responseObject)
                   {
                        moviePoster=(UIImageView *)[cell viewWithTag:101];
                       moviePoster.image=responseObject;
                   }failure:^(AFHTTPRequestOperation *operation,NSError *error)
                   {
                       NSLog(@"Error:%@",error);
                   }];
                  [operation start];
                  [showMovieDetailsArray addObject:picURL];
                  
                  //description
                  NSString *Description=json[@"Plot"];
//                  NSLog(@"%@",Description);
                  NSString *details=[NSString stringWithFormat:@"%@",Description];
                  [showMovieDescription addObject:details];
                  [showMovieDetailsArray addObject:details];
                  

                  //release date
                  NSString *releaseDate=json[@"Released"];
                  [showMovieReleaseDate addObject:releaseDate];
                  [showMovieDetailsArray addObject:releaseDate];
                  
                  //Actors
                  NSString *actors=json[@"Actors"];
                  NSString *actorsName=[NSString stringWithFormat:@"%@",actors];
                  [showMovieDetailsArray addObject:actorsName];
                  
                  //Awards
                  NSString *awards=json[@"Awards"];
                  NSString *awardsName=[NSString stringWithFormat:@"%@",awards];
                  [showMovieDetailsArray addObject:awardsName];
                  
                  //Country
                  NSString *country=json[@"Country"];
                  [showMovieDetailsArray addObject:country];
                  
                  //Director
                  NSString *director=json[@"Director"];
                  [showMovieDetailsArray addObject:director];
                  
                  //Genre
                  NSString *genre=json[@"Language"];
                  NSString *genreName=[NSString stringWithFormat:@"%@",genre];
                  [showMovieDetailsArray addObject:genreName];
                  
                  //Language
                  NSString *language=json[@"Genre"];
                  NSString *languagesName=[NSString stringWithFormat:@"%@",genre];
                  [showMovieDetailsArray addObject:languagesName];
                  
                  //rated
                  NSString *rated=json[@"Rated"];
                  NSString *ratedName=[NSString stringWithFormat:@"%@",rated];
                  [showMovieDetailsArray addObject:ratedName];
                  
                  //Writer
                  NSString *writer=json[@"Director"];
                  [showMovieDetailsArray addObject:writer];
                  
                  //rating
                  NSNumber *rating=json[@"imdbRating"];
                  [showMovieDetailsArray addObject:rating];
                  
                 [table reloadData];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error.description);
              NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);



          }];
    

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    if (count<10)
    {
        data=search.text;
        [ self getApi];
        count++;
        [showMovieDetailsArray removeAllObjects];
        [showMovieTitleArray removeAllObjects];
        [showMovieReleaseDate removeAllObjects];
    }
    else
    {
        [self alert1];
    }
    table.hidden=NO;
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return showMovieTitleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UILabel *titleLabel=(UILabel *)[cell viewWithTag:100];
    titleLabel.text =showMovieTitleArray[indexPath.row];
    
    
    UILabel *releaseDateLabel=(UILabel *)[cell viewWithTag:102];
    releaseDateLabel.text =showMovieReleaseDate[indexPath.row];
    
    
    return cell;
}


//Description in another view Controller

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier]isEqualToString:@"Go"])
    {
        DestinationController *dest=segue.destinationViewController;
        dest.destdata=showMovieDetailsArray;
    }
}

-(void)alert1{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Make A Paid Account" message:@"You Have searched more than 10 times " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
